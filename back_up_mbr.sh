#!/usr/bin/env bash
#created by: Ilay Ron
#purpose: back up mbr
#date: 21/10/20
#version: v1.0.0
##################################


main(){
echo="please enter your choice: "
options=("backup_mbr" "backup_hp" "backup_and_zip_hp" "backup_and_zip_wd" "clean_up_the_swap_partition")

#creating the menu

select opt in "${options[@]}"
do
	case $opt in
	"backup_mbr")
	back_up_mbr
	echo "your mbr backed up successfully"
	deco
;;
	"backup_hp")
	back_up_home_partition 
	echo "back up the home partiton succeeded "
	deco
;;
	"backup_and_zip_hp")
	back_up_hp_and_zip
	echo "home partition has been backup and clone into zip"
	deco
;;
	"bacup_and_zip_wd")
	backup_and_zip_whole_disk
	echo "The whole disk disk back up and zip successfully
	deco
;;
	"clean_up_the_swap_partition")
	back_up-swap_partition
	echo "you swap the partition successfully"
	deco	
;;
	
	"quit")
	break
	;;
	*)
esac
done

}
deco(){
_time=2.5
l="####################"
clear
printf "$l\n# %s\n$l" "$@"
sleep $_time
clear
}


back_up_mbr(){
#backing up MBR

dd if=/dev/sda of=/tmp/sda-mbr.bin bs=512 count=1 

#restoting partition table

dd if= sda-mbr.bin of=/dev/sda bs=1 count=64 skip=446 seek=446
}


back_up_home_partition(){


#copy the hone partition

sudo dd if=/dev/sda of-/dev/sdb1 bs=64k conv=noerror,sync

#Informed how much copied
sudo dd if=/dev/sda of=/dev/sdb1 bs=64k conv=noerror,sync status=progress

}


back_up_hp_and zip(){



ne partition

sudo dd if=/dev/sda of-/dev/sdb1 bs=64k conv=noerror,sync

#Informed how much copied
sudo dd if=/dev/sda of=/dev/sdb1 bs=64k conv=noerror,sync status=progress

#clone the file into zip
sudo dd if=/dev/sda conv=sync,noerror bs=64k | gzip -c > /home/Projects/backup_dd_file/backup_exe/backup_image.img.gz


}


backup_and_zip_whole_disk(){

#check the whole disks
df -h

#create the image

dd if=/dev/sda of=/nfs_test/sda_disk.img |p -c > /home/Projects/backup_dd_file/backup_exe/backup_image.img.gz 

#restore sda using disk image
dd if=/nfsshare/sda_disk.img of=/dev/sda

#reboot the server
shutdown -r now

}

back_up-swap_partition(){

#creating a partition
local disk_name="$1"

If [[ -z disk_mame ]];then

fdisk /dev/sdc >> EOL
n
p
1

+5ooM
p
t
p
w
EOL
else
mkswap /dev/sdc1

#activating swap partition
swapon /dev/sdc1

#mounting swap partition

mkdir /root/backuo
cp /etc/fstab /root/backup/
vim /etc/fstab
fi

}

main "$@"
